========
Install
========

Although the original ISA toolkit has been written in Matlab, we provide a lighter version in Python, with less tools,
but enough for the instance hardness analysis purposes. You may find the implementation in the separate package
`PyISpace <https://gitlab.com/ita-ml/pyispace>`_. Notwithstanding, the choice of the ISA engine is left up to the user,
which can be set in the configuration file. Below, we present the standard installation, and also the the additional
steps to configure the Matlab engine (optional).

Users
---------

PyHard is available in PyPi repository. You can install it as a standard package:

.. code-block::

   pip install pyhard


Developers
--------------

Alternatively, if you are a developer and want to contribute, the following installation is better suited for testing
new features:

.. code-block::

   git clone https://gitlab.com/ita-ml/pyhard.git
   cd pyhard
   pip install -e .


Conda environment
-----------------

Use the file `environment.yml <https://gitlab.com/ita-ml/pyhard/-/blob/master/environment.yml>`_ to create a conda env
with all the required dependencies:

.. code-block::

   conda env create --file environment.yml
