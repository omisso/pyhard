import numpy as np
import pandas as pd
import pytest
from sklearn.datasets import make_classification

from pyhard.validator import *


def test_target_classification_dtype(classification_dataset):
    data = classification_dataset
    is_target_dtype_valid('classification', data['target'].astype(int))
    is_target_dtype_valid('classification', data['target'].astype(str))

    with pytest.raises(AssertionError):
        is_target_dtype_valid('classification', data['target'].astype(float))


def test_target_regression_dtype(regression_dataset):
    data = regression_dataset
    is_target_dtype_valid('regression', data['target'].astype(float))
    is_target_dtype_valid('regression', data['target'].astype(int))

    with pytest.raises(AssertionError):
        is_target_dtype_valid('regression', data['target'].astype(str))


def test_numeric_features(classification_dataset):
    data = classification_dataset
    df_feat = data.drop(columns='target')
    are_features_numeric(df_feat)

    are_features_numeric(df_feat.astype(int))

    df_feat['str_col'] = data.iloc[:, 0].astype(str)
    with pytest.raises(AssertionError):
        are_features_numeric(df_feat)


def test_data_with_nan(classification_dataset):
    data = classification_dataset
    has_no_missing_values(data)

    data.iloc[[1, 3], [2, 4]] = np.nan
    with pytest.raises(AssertionError):
        has_no_missing_values(data)


def test_class_imbalance():
    _, y = make_classification(
        n_samples=1000,
        n_informative=10,
        n_classes=3,
        weights=None,
        flip_y=0
    )
    assert are_classes_balanced(pd.Series(y))

    _, y = make_classification(
        n_samples=1000,
        n_informative=10,
        n_classes=3,
        weights=[10/36, 11/36, 15/36],
        flip_y=0
    )
    assert are_classes_balanced(pd.Series(y))

    _, y = make_classification(
        n_samples=1000,
        n_informative=10,
        n_classes=3,
        weights=[9/36, 12/36, 15/36],
        flip_y=0
    )
    assert not are_classes_balanced(pd.Series(y))
