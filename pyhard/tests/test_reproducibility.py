import os
from pathlib import Path

import numpy as np
import pandas as pd
from joblib import Parallel, delayed

from pyhard.classification import ClassifiersPool
from pyhard.context import Configuration
from pyhard.feature_selection import featfilt
from pyhard.hpo import set_hyperopt_progressbar
from pyhard.integrator import build_metadata
from pyhard.measures import ClassificationMeasures


_my_path = Path(__file__).parent
_test_data = ['iris', '2normals', 'overlap', 'easy', 'mix']


def load_data(name):
    path = _my_path.parent / f'data/{name}/data.csv'
    df = pd.read_csv(path)
    df.index.name = 'Instances'
    return df


def data_generator(names):
    for name in names:
        df = load_data(name)
        yield df


def run_measures(data):
    df_feat = pd.DataFrame()
    flag = []
    for i in range(10):
        m = ClassificationMeasures(data)
        if i == 0:
            df_feat = m.calculate_all()
        else:
            df_feat_former = df_feat
            df_feat = m.calculate_all()
            flag.append(np.allclose(df_feat_former.values, df_feat.values))
    return flag


def run_classifiers(data, hpo=False):
    os.environ["PYHARD_SEED"] = "0"
    flag = []
    df_perf = pd.DataFrame()

    if hpo:
        set_hyperopt_progressbar(False)
        hpo_params = dict(hyper_param_optm=True, hpo_evals=10, hpo_timeout=60)
    else:
        hpo_params = dict(hyper_param_optm=False)

    with Configuration() as conf:
        params = conf.get('parameters')
    for i in range(4):
        c = ClassifiersPool(data)
        if i == 0:
            df_perf = c.run_all(n_folds=5, n_iter=1, parameters=params, **hpo_params)
        else:
            df_perf_former = df_perf
            df_perf = c.run_all(n_folds=5, n_iter=1, parameters=params, **hpo_params)
            flag.append(np.allclose(df_perf_former.values, df_perf.values))
    return flag


def run_fs(data):
    with Configuration() as conf:
        params = conf.get('parameters')
    kwargs = dict(n_folds=5, n_iter=1, parameters=params)
    df_metadata = build_metadata(data=data, return_ih=False, save=False, **kwargs)

    flag = []
    selected = None
    for i in range(5):
        if i == 0:
            selected, _ = featfilt(df_metadata, method='mrmr')
        else:
            selected_former = selected
            selected, _ = featfilt(df_metadata, method='mrmr')
            flag.append(len(set(selected) ^ set(selected_former)))
    return flag


def test_metafeature_reproducibility():
    result = Parallel(n_jobs=-1)(delayed(run_measures)(data) for data in data_generator(_test_data))
    print(result)
    assert np.all(list(map(np.all, result)))


def test_classification_reproducibility():
    result = Parallel(n_jobs=-1)(delayed(run_classifiers)(data) for data in data_generator(_test_data))
    print(result)
    assert np.all(list(map(np.all, result)))


def test_classification_hpo_reproducibility():
    result = Parallel(n_jobs=-1)(delayed(run_classifiers)(data, hpo=True) for data in data_generator(_test_data))
    print(result)
    assert np.all(list(map(np.all, result)))


def test_fs_reproducibility():
    result = Parallel(n_jobs=-1)(delayed(run_fs)(data) for data in data_generator(_test_data))
    print(result)
    assert sum(list(map(sum, result))) == 0
